# l-system

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

This is a simple tool to plot Lindenmayer system (l-system) which plots fractals given some rules and angle. Currently, this tool plots in 2-D.


## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Interface preview

Shows the plot of this dragon curve with the rules as shown below.

![alt tag](https://dl.dropboxusercontent.com/u/2920751/LSystem.png)

## Current Features

Able to pan, zoom in, zoom out, and rotate in 45 degree increments. And you can also download an image of the canvas.

## Future Additions

- Color settings for the plot and the background.
- Specify width for stroke.
- Specify colors at different levels.
